// destructuring react bootstrap components
import { Button, Row, Col } from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {Link, NavLink} from 'react-router-dom';

export default function Banner({banner}) {
	const {title, content, destination, label} = banner;
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button as={Link} to={destination} variant="primary">{label}</Button>
			</Col>
		</Row>
	)
}