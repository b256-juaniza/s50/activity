import './App.css';
import { Container } from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import { UserProvider } from './userContext';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import {useState, useEffect} from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Logout from './pages/Logout';
// Activity s53
import Error from './pages/Error';
import CourseView from './pages/CourseView';


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  })

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      {/* Self Closing Tags */}
      <AppNavBar />
      <Container>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/Courses" element={<Courses />} />
        <Route path="/Register" element={<Register />} />
        <Route path="/Login" element={<Login />} />
        <Route path="/Logout" element={<Logout />} />
        <Route path="/CourseView/:courseId" element={<CourseView />} />
        {/*  Activity s53 ----- */}
        <Route path="*" element={<Error />} />
      </Routes>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
