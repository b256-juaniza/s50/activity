import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
	const data = {
		title: "Zuitt Bootcamp",
		content: "Opportunities for everyone, everywhere.",
		destination: "/Courses",
		label: "Enroll Now!"
	}
	return (
		<>
			<Banner banner={data}/>
			<Highlights />
		</>
	)
}