import {useContext, useEffect} from 'react'; 
import { Navigate, useNavigate } from 'react-router-dom'; 
import UserContext from '../userContext';

export default function Logout() {
	const { unsetUser, setUser } = useContext(UserContext);
	unsetUser();
	useEffect(() => {
		setUser({
			email: null,
			id: null
		})
	})
	return (
		<Navigate to="/Login" />
	)
}
