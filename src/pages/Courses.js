import { useState, useEffect } from 'react';
// import CourseData from '../data/CourseData';
import CourseCard from '../components/CourseCard';

export default function Courses() {

	const [courses, setCourses] = useState([])
	
	useEffect(() => {
		fetch('http://localhost:4000/courses/active')
		.then(res => res.json())
		.then(data => {

			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
				)
			}))
		})
	}) 

	return (
		<>
			<h1>Courses</h1>
			{ courses }
		</>
	)
}