import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../userContext';
import { Navigate, useNavigate } from 'react-router-dom';
import swal from 'sweetalert2';

export default function Login() {
	const {user, setUser} = useContext(UserContext);
	const [ct, setCt] = useState(0);
	let i = ct;
	
	if (user.id !== null && i+1 == ct) {
		setCt(ct+1);
	}
	const navigate = useNavigate();

	const [email, setEmail] = useState("");
	const [Password, setPassword] = useState("");
	const [button, setButton] = useState(false);

	useEffect(() => {
		if (email !== '' && Password !== '') {
			setButton(true);
		}
	})

	function login(e) {

		e.preventDefault();

		fetch('http://localhost:4000/users/login', {
			method: "POST",
			headers: {
				'Content-Type' : 'Application/json'
			},
			body: JSON.stringify({
				email: email,
				password: Password
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				swal.fire({
					title: "Authentication Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
				})

			navigate("/Courses")

			} else {
				swal.fire({
					title: "Authentication Failed!",
					icon: "error",
					text: "Check your login details and try again!"
				})
			}
		})

	/*	localStorage.setItem("email", email)

		setUser({
			email: localStorage.getItem('email')
		})*/

		setEmail('');
		setPassword('');
		// alert("Successfully Log-in!");

	}

	const retrieveUserDetails = (token) => {

		fetch('http://localhost:4000/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})

	}

	return (
		(user.id !== undefined && user.id !== null) ?
			<Navigate to="/Courses" />
			:
	    <Form onSubmit={e => login(e)}>
	      <Form.Group className="mb-3" controlId="formBasicEmail">
	        <Form.Label>Email:</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Password:</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={Password} onChange={e => setPassword(e.target.value)}/>
	      </Form.Group>
	      {
	      	button ?
	      	<Button variant="primary" type="submit">
	        Submit
	      	</Button>
	      	:
	      	<Button variant="warning" type="submit" disabled>
	        Submit
	      	</Button>
	      }
	    </Form>
	)
}