import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../userContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){

	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();
	const [email, setEmail] = useState("");
	const [fname, setFname] = useState("");
	const [lname, setLname] = useState("");
	const [number, setNumber] = useState("");
	const [Password1, setPassword1] = useState("");
	const [Password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	console.log(email);

	useEffect (() => {
		if ((email !== '' && Password1 !== '' && Password2 !== '' && fname !== '' && lname !== '' && number !== '') && (Password1 === Password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
		fetch('http://localhost:4000/users/checkEmail', {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json',
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if (data) {
				Swal.fire({
				title: "Email is already taken",
				icon: "error",
				text: "Please try again."
				})
			}
		})
	})

	function registerUser(e) {
		e.preventDefault();

		localStorage.setItem("email", email)

		fetch('http://localhost:4000/users/register', {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json',
			},
			body: JSON.stringify({
				firstName : fname,
				lastName : lname,
				email : email,
				mobileNo : number,
    			password : Password2
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				Swal.fire({
					title: "Successfully Enrolled!",
					icon: "success",
					text: "You have Successfully enrolled for this course."
				})
			navigate("/login");
			}
		})

		setFname('');
		setLname('');
		setEmail('');
		setNumber('');
		setPassword1('');
		setPassword2('');
	}

	return (
		<Form onSubmit={e => registerUser(e)}>
	      <Form.Group className="mb-3" controlId="formBasicEmail">
	       	<Form.Label>First Name: </Form.Label>
	        <Form.Control type="text" placeholder="First Name" value={fname} onChange={e => setFname(e.target.value)}/>
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="text" placeholder="Last Name" value={lname} onChange={e => setLname(e.target.value)}/>
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	        <br/>
	         <Form.Label>Mobile Number: </Form.Label>
	        <Form.Control type="number" placeholder="09123456789" value={number} onChange={e => setNumber(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={Password1} onChange={e => setPassword1(e.target.value)}/>
	      </Form.Group>
	      <Form.Group className="mb-3" controlId="formBasicPassword2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control type="password" placeholder="Verify Password" value={Password2} onChange={e => setPassword2(e.target.value)}/>
	      </Form.Group>
	      {
	      	isActive ?
	      	<Button variant="primary" type="submit" id="submitBtn">
	        Submit
	      	</Button>
	      	:
	      	<Button variant="danger" type="submit" id="submitBtn" disabled>
	        Submit
	      	</Button>
	      }
	    </Form>
	)
}